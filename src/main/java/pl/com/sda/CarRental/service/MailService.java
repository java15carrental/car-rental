package pl.com.sda.CarRental.service;

import pl.com.sda.CarRental.model.BorrowedDate;
import pl.com.sda.CarRental.model.Car;
import pl.com.sda.CarRental.model.Customer;

public interface MailService {

    void sendMailTest();

    void sendMail(Customer customer, BorrowedDate borrowedDate, Car car);
}
