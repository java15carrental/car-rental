package pl.com.sda.CarRental.service;

import pl.com.sda.CarRental.model.AvailableCarsResult;
import pl.com.sda.CarRental.model.BorrowedDate;

import java.util.Calendar;
import java.util.List;

public interface BorrowedDateService {

    List<AvailableCarsResult> checkAvailableCarById(Calendar startDate, Calendar endDate, Long id);

    List<AvailableCarsResult> checkAvailableCars(Calendar startDate, Calendar endDate);

    List<BorrowedDate> findAll();

    BorrowedDate findByCustomerId(Long id);

    BorrowedDate findByCarId(Long id);

    void save(BorrowedDate borrowedDate);

    long countDays(BorrowedDate borrowedDate);
}
