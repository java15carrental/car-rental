package pl.com.sda.CarRental.service;


import org.springframework.stereotype.Service;
import pl.com.sda.CarRental.model.AvailableCarsResult;
import pl.com.sda.CarRental.model.BorrowedDate;
import pl.com.sda.CarRental.repository.BorrowedDateDao;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class BorrowedDateServiceImpl implements BorrowedDateService {


    private BorrowedDateDao borrowedDateDao;

    public BorrowedDateServiceImpl(BorrowedDateDao borrowedDateDAO) {
        this.borrowedDateDao = borrowedDateDAO;
    }

    @Override
    public BorrowedDate findByCustomerId(Long id) {
        return this.borrowedDateDao.findByCustomerId(id);
    }

    @Override
    public BorrowedDate findByCarId(Long id) {
        return this.borrowedDateDao.findByCarId(id);
    }

    @Override
    public List<BorrowedDate> findAll() {
        return this.borrowedDateDao.findAll();
    }

    @Override
    public void save(BorrowedDate borrowedDate) {
        this.borrowedDateDao.save(borrowedDate);
    }

    @Override
    public List<AvailableCarsResult> checkAvailableCars(Calendar startDate, Calendar endDate) {
        return this.borrowedDateDao.checkAvailableCars(startDate, endDate);
    }

    @Override
    public List<AvailableCarsResult> checkAvailableCarById(Calendar startDate, Calendar endDate, Long id) {
        return this.borrowedDateDao.checkAvailableCarById(startDate, endDate, id);
    }

    @Override
    public long countDays(BorrowedDate borrowedDate) {
        long days;
        Calendar start = borrowedDate.getStartDate();
        Calendar end = borrowedDate.getEndDate();
        days = daysBetween(start, end);
        return days;
    }

    private long daysBetween(Calendar startDate, Calendar endDate) {
        endDate.add(Calendar.DATE, 1);
        long end = endDate.getTimeInMillis();
        long start = startDate.getTimeInMillis();
        return TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
    }
}
