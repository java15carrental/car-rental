package pl.com.sda.CarRental.service;

import pl.com.sda.CarRental.model.Car;

import java.util.List;

public interface CarService {
    List<Car> findAll();

    List<Car> newCars();

    Car findById(Long id);

    void save(Car car);
}
