package pl.com.sda.CarRental.service;

import org.springframework.stereotype.Service;
import pl.com.sda.CarRental.model.Customer;
import pl.com.sda.CarRental.repository.CustomerDAO;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerDAO customerDAO;

    public CustomerServiceImpl(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

    @Override
    public Customer findById(Long id) {
        return this.customerDAO.findById(id);
    }

    @Override
    public List<Customer> findAll() {
        return this.customerDAO.findAll();
    }

    @Override
    public void save(Customer customer) {
        this.customerDAO.save(customer);
    }
}
