package pl.com.sda.CarRental.service;

import pl.com.sda.CarRental.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> findAll();

    Customer findById(Long id);

    void save(Customer customer);
}
